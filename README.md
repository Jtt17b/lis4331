> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 4331 Advanced Mobile Applications Development

## Jordan Thau

### LIS4331 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file"
)	- Install JDK
	- Install Android Studio and create My First App and Contacs App
	- Provide screenshots of installations
	- Create Bitbucket repo
	- Complete bitbucket tutorial (bitbucketstationlocation)
	- Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Drop down menus for number of guests and tip percentages
	- Custom background color/theme
	- Display custom launcher icon image

3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Toast notification
	- Radio buttons
	- Correct currency symbols
	- Splash loading screen
	- Screenshots and skillsets
4. [P1 README.md](p1/README.md "My P1 README.md file")
	- Artists and Media
	- Properly aligned buttons
	- Splash loading screen
	- Screenshots and skillsets
5. [A4 README.md](a4/README.md "My A4 README.md file")
	- Persistent Data
	- Properly aligned widgets/images
	- Splash loading screen
	- Screenshots and skillsets
5. [A5 README.md](a5/README.md "My A5 README.md file")
	- Main screen with app title and articles
	- Own RSS feed
	- Own theme
	- Launcher Icon
	- Skillsets 
6. [P2 README.md](p2/README.md "My P2 README.md file")
	- Splash Screen
	- Persistent Data
	- Own theme
	- Insert 5 Users
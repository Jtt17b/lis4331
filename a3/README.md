> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advanced Mobile Web Application Development

## Jordan Thau

### Assignment 2 Requirements:

1. Toast notification
2. Radio buttons
3. Correct currency symbols
4. Splash loading screen
5. Screenshots and skillsets
#### README.md file should include the following items:

* Screenshot running application's unpopulated interface
* Screenshot running application's populated interface
* Screenshot of splash screen
* Screenshot of skillsets

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:
| *Screenshot of Unpopulated* | *Screenshot of populated*: | *Screenshot of toast* | *Screenshot of Splash*i|
|-|-|-|-|
| ![a](https://bitbucket.org/Jtt17b/lis4331/raw/23b4bcc3b68652f65fee4db241a6e543dc825778/a3/img/A3Second.PNG) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/23b4bcc3b68652f65fee4db241a6e543dc825778/a3/img/A3Third.PNG) |![a](https://bitbucket.org/Jtt17b/lis4331/raw/0a425e405606d972980a65074271a37fdd2fc0f7/a3/img/A3FOURTH.PNG) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/e8c4fd94fec4fe0660bf0502b15116e1621052c9/a3/img/Splash.PNG)

|*SS4*|*SS4*|*SS5*|
|-|-|-|
| ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/ec49c70793e48ade3b9abf0386b1665119b527ef/a3/img/TimeConversion1.PNG) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/ec49c70793e48ade3b9abf0386b1665119b527ef/a3/img/Timeconversion2.PNG) |![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/ec49c70793e48ade3b9abf0386b1665119b527ef/a3/img/DataValid1.PNG)
|*SS5*|*SS5*|*SS5*|
![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/ec49c70793e48ade3b9abf0386b1665119b527ef/a3/img/DataValid0.PNG) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/ec49c70793e48ade3b9abf0386b1665119b527ef/a3/img/DataValid4.PNG) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/995cc5813b10a8db027769da8dea2dafaa2fa6ca/a3/img/INVALID.PNG) 
|*SS6*|*SS6*|
![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/03f7f51c2a3e4d712fb8c62c24f3351bc73a7234/a3/img/PCalc0.PNG) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/03f7f51c2a3e4d712fb8c62c24f3351bc73a7234/a3/img/PCalc1.PNG) 

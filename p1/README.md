> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advanced Mobile Web Application Development

## Jordan Thau

### Assignment 2 Requirements:

1. Artists and Media
2. Properly aligned buttons
3. Splash loading screen
4. Screenshots and skillsets
#### README.md file should include the following items:

* Screenshot running application's splash screen
* Screenshot running application's follow up screen
* Screenshot of play screen
* Screenshot of pause screen

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:
| *Screenshot of Splash* | *Screenshot of Opening*: | *Screenshot of Playing* | *Screenshot of Pause*|
|-|-|-|-|
| ![a](https://bitbucket.org/Jtt17b/lis4331/raw/74cec6ee04da4016e2995db2fc08fe609d511315/p1/img/SplashScreen.PNG) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/74cec6ee04da4016e2995db2fc08fe609d511315/p1/img/Opening.PNG) |![a](https://bitbucket.org/Jtt17b/lis4331/raw/74cec6ee04da4016e2995db2fc08fe609d511315/p1/img/Playing.PNG) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/74cec6ee04da4016e2995db2fc08fe609d511315/p1/img/PauseScreen.PNG)

|*SS7*|*SS7*|*SS8*|
|-|-|-|
| ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/1e651b2e829581da0c98be112451841881ead291/p1/img/MeasurementConversionRunning.PNG) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/1e651b2e829581da0c98be112451841881ead291/p1/img/MeasurementSource.PNG) |![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/1e651b2e829581da0c98be112451841881ead291/p1/img/Distance1.PNG)
|*SS8*|*SS8*|*SS9*|
![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/1e651b2e829581da0c98be112451841881ead291/p1/img/Distance2.PNG) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/1e651b2e829581da0c98be112451841881ead291/p1/img/DistanceSource.PNG) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/1e651b2e829581da0c98be112451841881ead291/p1/img/Multiple1.PNG) 
|*SS9*|*SS9*|*SS9*|
![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/1e651b2e829581da0c98be112451841881ead291/p1/img/Multiple2.PNG) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/1e651b2e829581da0c98be112451841881ead291/p1/img/Multiple3.PNG) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/1e651b2e829581da0c98be112451841881ead291/p1/img/MultipleSource.PNG) 


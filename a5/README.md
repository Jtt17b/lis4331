> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advanced Mobile Web Application Development

## Jordan Thau

### Assignment 5 Requirements:

1. Main screen with app title and articles
2. Own RSS feed
3. Own theme
4. Launcher Icon
#### README.md file should include the following items:

* Screenshot running application's main screen
* Screenshot running application's article screen
* Screenshot of browser

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:
| *Screenshot of Main* | *Screenshot of Article*: | *Screenshot of Browser* |
|-|-|-|-|
| ![a](https://bitbucket.org/Jtt17b/lis4331/raw/d16722ee84b5fd86972440ed0db3205e0c587bb5/a5/img/RSS%20Screen.PNG) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/d16722ee84b5fd86972440ed0db3205e0c587bb5/a5/img/DESC.PNG) |![a](https://bitbucket.org/Jtt17b/lis4331/raw/d16722ee84b5fd86972440ed0db3205e0c587bb5/a5/img/Browser.PNG) 
|*SS13*|*SS14*|*SS14*|
|-|-|-|
| ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/5ed0e6fc2efba43ca5fd769b23646eb4c913e7ed/a5/img/SS13.PNG) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/5ed0e6fc2efba43ca5fd769b23646eb4c913e7ed/a5/img/SS14Source.PNG) |![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/5ed0e6fc2efba43ca5fd769b23646eb4c913e7ed/a5/img/SS14Run.PNG)
|*SS15*|*SS15*|
|-|-|
![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/5ed0e6fc2efba43ca5fd769b23646eb4c913e7ed/a5/img/SS15Sourec.PNG) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/5ed0e6fc2efba43ca5fd769b23646eb4c913e7ed/a5/img/SS15Run.PNG) 

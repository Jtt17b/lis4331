> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advanced Mobile Web Application Development

## Jordan Thau

### Assignment 2 Requirements:

*Four Parts:*

1. Drop down menu for number of guests
2. Drop down menu for tip percentages
3. Custom background color/theme
4. Display custom launcher icon image

#### README.md file should include the following items:

* Screenshot running application's unpopulated interface
* Screenshot running application's populated interface
* Screenshot of skillsets

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:
| *Screenshot of populated*: | *Screenshot of unpopulated* | 
|--|--|
| ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/094fc897608fcb0e111ded032e919464e8045637/a2/img/populated.png) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/094fc897608fcb0e111ded032e919464e8045637/a2/img/unpopulated.png)

|*SS1*|*SS2*|*SS3*|
|-|-|-|
| ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/7c073e798822ec6cb03d24036db7ea3da3014ad7/a2/img/SS1.PNG) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/7c073e798822ec6cb03d24036db7ea3da3014ad7/a2/img/SS3.PNG) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/7c073e798822ec6cb03d24036db7ea3da3014ad7/a2/img/SS2.PNG)




> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advanced Mobile Web Application Development

## Jordan Thau

### Assignment 4 Requirements:

1. Persistent Data
2. Properly aligned widgets/images
3. Splash loading screen
4. Screenshots and skillsets
#### README.md file should include the following items:

* Screenshot running application's splash screen
* Screenshot running application's main screen
* Screenshot of invalid screen
* Screenshot of valid screen

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:
| *Screenshot of Splash* | *Screenshot of Main*: | *Screenshot of Invalid* | *Screenshot of Valid*|
|-|-|-|-|
| ![a](https://bitbucket.org/Jtt17b/lis4331/raw/e6873b792f90c7b0afe432eb6777df12d52688fe/a4/img/splash.PNG) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/e6873b792f90c7b0afe432eb6777df12d52688fe/a4/img/main.PNG) |![a](https://bitbucket.org/Jtt17b/lis4331/raw/e6873b792f90c7b0afe432eb6777df12d52688fe/a4/img/incorrect.PNG) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/e6873b792f90c7b0afe432eb6777df12d52688fe/a4/img/correct.PNG)

|*SS10*|*SS10*|*SS11*|
|-|-|-|
| ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/ad5dd695b7c2537273b44628f62a823d621006c7/a4/img/TravelExample.PNG) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/ad5dd695b7c2537273b44628f62a823d621006c7/a4/img/TravelSource.PNG) |![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/ad5dd695b7c2537273b44628f62a823d621006c7/a4/img/ExampleProduct.PNG)
|*SS11*|*SS11*|*SS12*|
![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/ad5dd695b7c2537273b44628f62a823d621006c7/a4/img/ProductSource.PNG) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/ad5dd695b7c2537273b44628f62a823d621006c7/a4/img/DemoSourec.PNG) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/ad5dd695b7c2537273b44628f62a823d621006c7/a4/img/BookDemo.PNG) 
|*SS12*|*SS12*|
![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/ad5dd695b7c2537273b44628f62a823d621006c7/a4/img/BookSource.PNG) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/ad5dd695b7c2537273b44628f62a823d621006c7/a4/img/BookDemoSource.PNG)


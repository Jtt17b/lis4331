> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advanced Mobile Web Application Development

## Jordan Thau

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1,2)
4. Bitbucket repo links a) this assignment and b) the completed tutorials above (bitbucketstationlocations)

#### README.md file should include the following items:

* Screenshot of running Java Hello 
* Screenshot of running Android Studio - My first App
* Screenshots of running Android Studio - Contacts App
* git commands w/ short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Creates an empty git repository
2. git status - Shows the current status of the working tree
3. git add - Adds a file to the index to be tracked
4. git commit - Record file changes
5. git push - Send the committed file changes
6. git pull - Fetch and integrate files
7. git clone - Copy a repository to a specified directory

#### Assignment Screenshots:
| *Screenshot of java hello*: | *Screenshot of first app* | 
|---|---|
| ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/25da54693b2cb28cf87759df10e89b9e2b965110/a1/img/javahello.PNG){width=100px height=200px} | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/25da54693b2cb28cf87759df10e89b9e2b965110/a1/img/firstapp.PNG =200x50)

|*Screenshot of home page*: | *Screenshot of a contact* |
|---|---|
| ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/25da54693b2cb28cf87759df10e89b9e2b965110/a1/img/first.PNG) | ![JDK Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/6814a79ff221cdc1abb12beca0a76585cad4fc5a/a1/img/second.PNG)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jtt17b/bitbucketstationlocations/src/master "Bitbucket Station Locations")


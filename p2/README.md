> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advanced Mobile Web Application Development

## Jordan Thau

### Project 2 Requirements:
1. Splash Screen
2. Persistent Data
3. Own theme
4. Insert 5 Users
#### README.md file should include the following items:

* Screenshot running application's splash screen
* Screenshot running application's add screen
* Screenshot running application's update screen
* Screenshot running application's delete screen
* Screenshot running application's view screen


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:
| *Screenshot of Splash* | *Screenshot of Add* | *Screenshot of Update*  | *Screenshot of Delete*  | *Screenshot of Views* |
|-|-|-|-|-|
| ![a](https://bitbucket.org/Jtt17b/lis4331/raw/c48d15c32b1c57c5448b28b7441e68058d65c73d/p2/img/Splash.PNG) | ![AMPPS Installation Screenshot](https://bitbucket.org/Jtt17b/lis4331/raw/c48d15c32b1c57c5448b28b7441e68058d65c73d/p2/img/Add.PNG) |![a](https://bitbucket.org/Jtt17b/lis4331/raw/c48d15c32b1c57c5448b28b7441e68058d65c73d/p2/img/Delete.PNG) |![a](https://bitbucket.org/Jtt17b/lis4331/raw/c48d15c32b1c57c5448b28b7441e68058d65c73d/p2/img/Update.PNG) |![a](https://bitbucket.org/Jtt17b/lis4331/raw/4589cd50a6c94b9656f95e3285f55fe7ae15e632/p2/img/ViewWithUsers.PNG) 